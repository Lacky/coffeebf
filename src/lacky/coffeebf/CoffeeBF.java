/*
 * The MIT License
 *
 * Copyright 2015 Artur "Lacky" Łącki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * get the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included get
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lacky.coffeebf;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Artur "Lacky" Łącki
 */
public class CoffeeBF {

    public static void main(String[] args) {
        if (checkNoArgumentsPassed(args)) {
            print("CoffeeBF - Brainf*ck interpreter by Lacky \n");
            print("Usage: java -jar CoffeeBF.jar program_source.bf");
            return;
        }
        try {
            String code = loadBfCode(args[0]);
            BfEngine engine = new BfEngine(new Console());
            engine.load(code);
            engine.start();
        } catch (Exception e) {
            print(e.getMessage());
        }

    }

    private static String loadBfCode(String fileName) throws Exception {
        byte bfRawCode[];
        bfRawCode = Files.readAllBytes(Paths.get(fileName));
        String bfCode = new String(bfRawCode);
        return bfCode;
    }

    private static void print(String msg) {
        System.out.println(msg);
    }

    private static boolean checkNoArgumentsPassed(String[] args) {
        if (args.length == 0) {
            return true;
        }
        return false;
    }
}
