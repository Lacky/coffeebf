/*
 * The MIT License
 *
 * Copyright 2015 Artur "Lacky" Łącki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * get the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included get
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lacky.coffeebf;

import java.util.Arrays;

/**
 *
 * @author Artur "Lacky" Łącki
 */
public class BfEngine {

    private static final int MAX_MEM_SIZE = 30000;
    private static final byte memory[] = new byte[MAX_MEM_SIZE];
    private int pointer;
    private int codeSize;
    private final Stream channel;
    private String code;

    public BfEngine(Stream channel) {
        this.channel = channel;
    }

    public void load(String bfCode) {
        codeSize = bfCode.length();
        code = bfCode;
        Arrays.fill(memory, (byte) 0);
        pointer = 0;
    }

    public void start() throws Exception {
        //pc means Program Counter
        for (int pc = 0; pc < codeSize; pc++) {
            switch (code.charAt(pc)) {
                case '>':
                    if (pointer < MAX_MEM_SIZE - 1) {
                        incPointer();
                    }
                    break;
                case '<':
                    if (pointer > 0) {
                        decPointer();
                    }
                    break;
                case '+':
                    incValue();
                    break;
                case '-':
                    decValue();
                    break;
                case '.':
                    channel.print((char) memory[pointer]);
                    break;
                case ',':
                    memory[pointer] = channel.get();
                    break;
                case '[':
                    pc = openBracket(pc);
                    if (pc == -1) {
                        throw new Exception("Cannot find a closing bracket ']'");
                    }
                    break;
                case ']':
                    pc = closeBracket(pc);
                    if (pc == -1) {
                        throw new Exception("Cannot find a opening bracket '['");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void incPointer() {
        pointer++;
    }

    private void decPointer() {
        pointer--;
    }

    private void incValue() {
        memory[pointer]++;
    }

    private void decValue() {
        memory[pointer]--;
    }

    private int openBracket(int pc) {
        if (memory[pointer] != 0) {
            return pc;
        }

        int depth = 0;

        for (int i = pc + 1; i < codeSize; i++) {
            if (code.charAt(i) == '[') {
                depth++;
            } else if (code.charAt(i) == ']') {
                if (depth == 0) {
                    return i;
                } else {
                    depth--;
                }
            }
        }

        return -1;
    }

    private int closeBracket(int pc) {
        int depth = 0;

        for (int i = pc - 1; i > 0; i--) {
            if (code.charAt(i) == ']') {
                depth++;
            } else if (code.charAt(i) == '[') {
                if (depth == 0) {
                    return i - 1;
                } else {
                    depth--;
                }
            }
        }
        return -1;
    }
}
