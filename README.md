# CoffeeBF #

Brainf*ck interpreter written in Java. Compiled version is available [here](https://copy.com/eVLNLmHJIZJcVCro). Project created only for educational purpose (learning of Java). In case of any problems or suggestions write message to me: alacki93 at gmail.

### How use it? ###

CoffeBF requires Java installed in your system. To run BF script you must just type in system console:
*java -jar CoffeeBF.jar path_to_bf_file*

### License ###

All source code is distributed under [MIT license](http://opensource.org/licenses/MIT). 